<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// ini klo ndak pakai controller
// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/welcome', function () {
//     return view('welcome');
// });


// ini pakai COntroller

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@submit');
