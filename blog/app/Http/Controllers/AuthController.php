<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function submit(request $request)
    {
        //dd($request->all()); // buat testing yang di kirim tombol submit masuk apa ndak.. get 'name' nya array
        //karna ga pakai database.. kita buat dulu variabel untuk penampungan sementara data yang di kirim
        $first_name = $request['firstname']; //ngambil index dari penamaan name = 'firstname' di register.blade.php
        $last_name = $request['lastname'];
        return view('welcome', compact('first_name', 'last_name'));
    }
}
